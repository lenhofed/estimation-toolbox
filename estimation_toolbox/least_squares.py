import copy
import sys

import lmfit
import numpy as np
import scipy
from lmfit.minimizer import MinimizerResult
from scipy.stats import norm
# from typing import override

from estimation_toolbox.estimation_process import EstimationProcess

class LeastSquares(EstimationProcess):
    """
    This class implements least-squares mixin to be used with a defined model
    This has the advantage of converging faster than lmfit and to allow normal equation manipulation.
    """

    def __init__(self, *args, sig0=1, **kwargs):
        """
        :param sig0: approximated chi-square value, defaults to 1
        :type sig0: float, optional
        """

        self.normal_equation = None
        self.right_hand_member = None

        self.iteration_number = 0
        self.sig0 = sig0
        
        super().__init__(*args, **kwargs)


    """
    Defining matrices
    """

    def weight(self, except_rows=None):
        """
        Function of the weighting matrix

        :return: Weighting matrix
        :rtype: Array
        """
        return np.diag(1 / self.get_observations_variance(except_rows=except_rows))

    """
    Handling constraints
    """

    # @override
    def remove_constraints(self):
        """
        Removes applied constraints

        :return:
        """
        Nc, Kc = self._get_constraints_matrices(input_parameters=self.init_parameters)

        if self.normal_equation is not None:
            self.normal_equation -= Nc
        if self.right_hand_member is not None:
            self.right_hand_member -= Kc

        self.constraints = []
        self.n_const = len(self.constraints)

    # @override
    def add_constraints(self, const):
        """
        Adds constraints to the model

        :param const: input constraints
        :type const: list[dict]
        :return:
        """
        new_const = copy.deepcopy(self.constraints) + const
        if self.right_hand_member is not None:

            self.remove_constraints()
            self.constraints = new_const
            Nc, Kc = self._get_constraints_matrices(input_parameters=self.init_parameters)  # when estimation is finished, right end member is expressed from X0
            self.normal_equation += Nc
            self.right_hand_member += Kc

        else:
            self.constraints = new_const

        self.n_const = len(self.constraints)

    # @override
    def _get_constraints_matrices(self, input_parameters=None):
        """
        Computes constraint matrices
        :param input_parameters: Parameters to use for constraints definition, defaults to None
        :type input_parameters: lmfit.Parameters, optional

        :return:
            - **Nc** (Array) normal equation
            - **Kc** (Array) right end member
        """
        if len(self.constraints) != 0:

            Bc, Ac, Wc = super()._get_constraints_matrices(input_parameters=input_parameters)  # when estimation is finished, right end member is expressed from X0
            Wc = np.diag(self.sig0 ** 2 / Wc)
            Nc = np.transpose(Ac) @ Wc @ Ac
            Kc = np.transpose(Ac) @ Wc @ Bc

            return Nc, Kc

        else:
            return 0, 0

    """
    Least-squares estimation
    """

    def linear_lse(self, inversion_method="simple", verbosity=0):
        """
        Run linear least-squares estimation

        :param inversion_method: method of inversion (simple/sparse/cholesky/qr), defaults to "simple"
        :type inversion_method: str, optional
        :param verbosity: level of verbosity (0, 1, 2), defaults to 0
        :type verbosity: int, optional
        """

        self.non_linear_lse(0, 1, 1, 1, inversion_method, verbosity)

    def non_linear_lse(self, max_delta=1e-8, max_iteration=10, iteration=1, learning_rate=1, inversion_method="simple",
                       verbosity=0, remove_outliers_per_iter=None):
        """
        Run non-linear least-squares estimation

        :param max_delta: Maximum threshold between consecutive iterations, defaults to 1e-8
        :type max_delta: float, optional
        :param max_iteration: maximum number of iteration, defaults to 10
        :type max_iteration: int, optional
        :param iteration: Iteration number, defaults to 1
        :type iteration: int, optional
        :param learning_rate: LSE learning rate at each step, defaults to 1
        :type learning_rate: float, optional
        :param inversion_method: method of inversion (simple/sparse/cholesky/qr), defaults to "simple"
        :type inversion_method: str, optional
        :param verbosity: level of verbosity (0, 1, 2), defaults to 0
        :type verbosity: int, optional
        :param remove_outliers_per_iter: outliers automatic detection method to apply on residuals for given iterations, defaults to None
        :type remove_outliers_per_iter: dict[int: method], optional
        :return:

        .. math::
            \delta X = (J^T W J)^{-1} J^T W B

        .. note::
            Choosing a learning rate between 0 and 1 might be useful in case of divergence or zigzagging convergence (see https://en.wikipedia.org/wiki/Gauss%E2%80%93Newton_algorithm)
        """

        print("         # Iteration n° " + str(iteration))

        if (remove_outliers_per_iter is not None and iteration in remove_outliers_per_iter.keys()):
            self.detect_outliers(remove_outliers_per_iter[iteration], verbosity)

        # vector of parameters
        Xk = np.array([self.parameters[p].value for p in self.parameters])
        Xk = Xk.reshape(Xk.size, 1)

        # vector of observation
        B = self.func_fit(except_rows=self.detected_outliers_idx)
        # if len(self.detected_outliers_idx) > 0:
        #     B = np.delete(B, self.detected_outliers_idx)
        B = B.reshape(B.size, 1)

        # matrix of partial derivatives
        A = self.jacobian(except_rows=self.detected_outliers_idx)
        # if len(self.detected_outliers_idx) > 0:
        #     A = np.delete(A, self.detected_outliers_idx, axis=0)
        At = np.transpose(A)

        # matrix of weight
        P = self.weight(except_rows=self.detected_outliers_idx)
        P *= self.sig0 ** 2
        # if len(self.detected_outliers_idx) > 0:
        #     P = np.delete(P, self.detected_outliers_idx, axis=0)
        #     P = np.delete(P, self.detected_outliers_idx, axis=1)

        if verbosity >= 2:
            print("         * Xk : \n {}".format(Xk))
            print("         * B : \n {}".format(B))
            print("         * A : \n {}".format(A))
            print("         * P : \n {}".format(P))

        N = np.dot(np.dot(At, P), A)
        K = np.dot(np.dot(At, P), B)
        Nc, Kc = self._get_constraints_matrices()  # on the fly, right end member is expressed from Xk

        if verbosity >= 2:
            print("         * N : \n {}".format(N))
            print("         * Nc : \n {}".format(Nc))
            print("         * K : \n {}".format(K))
            print("         * Kc : \n {}".format(Kc))

        N += Nc
        K += Kc

        if inversion_method == "qr":

            Q, R = np.linalg.qr(A)
            dX, Qx = self.inverse_normal_equation(R, np.dot(np.transpose(Q), B), inversion_method, verbosity)

        else:
            dX, Qx = self.inverse_normal_equation(N, K, inversion_method, verbosity)

        # convergence is achieved
        if np.max(np.abs(dX)) <= max_delta or iteration >= max_iteration:

            print("         * End iterations and save results")
            if verbosity >= 1:
                print("             --> Reached minimum update : {}".format(np.max(np.abs(dX)) <= max_delta))
                print("             --> Reached maximum iteration : {}".format(iteration >= max_iteration))

            # save results
            self.update_params(Xk + dX, Qx)
            self.iteration_number = iteration
            self.estimate_vector = Xk + dX
            self.estimate_covar = Qx

            # save normal equations so that it can recompute solution easily
            self.normal_equation = N
            self.right_hand_member = K - np.dot(N, self.init_vector.reshape(Xk.size, 1) - Xk)
            # !! The right hand member is expressed to have dX = N-1 K = X - X0

            # compute statistics
            self.residuals = self.func_fit(except_rows=self.detected_outliers_idx)
            self.compute_statistics()

        # continue iterating
        else:

            self.update_params(Xk + learning_rate * dX, Qx)
            self.non_linear_lse(max_delta, max_iteration, iteration + 1, learning_rate, inversion_method,
                                verbosity, remove_outliers_per_iter)

    def inverse_normal_equation(self, N, K, inversion_method, verbosity=0):
        """
        Computes solution from matrices

        :param N: normal equation
        :type N: Array
        :param K: right end member
        :type K: Array
        :param inversion_method: inversion method to use for N
        :type inversion_method: str
        :param verbosity: level of verbosity (0, 1, 2), defaults to 0
        :type verbosity: int, optional
        :returns:
            - **dX** (Array) parameters update
            - **Qx** (Array) parameters uncertainties

        :raise ValueError: if inversion method not implemented

        .. note:: for factorization methods see http://www.math.iit.edu/~fass/477577_Chapter_5.pdf
        """
        N, K = N.astype(float), K.astype(float)  # ensure dtype is float64

        if verbosity >= 1:
            print("         * Inverse normal matrix by {} method".format(inversion_method))
            print("         * Condition number : ", np.linalg.cond(N))

        if inversion_method == "simple":
            Qx = np.linalg.inv(N)
            dX = np.linalg.solve(N, K)

        elif inversion_method == "sparse":
            Qx = scipy.sparse.linalg.inv(scipy.sparse.csc_matrix(N)).toarray()
            dX = np.dot(Qx, K)

        elif inversion_method == "cholesky":
            L = np.linalg.cholesky(N)
            L_inv = np.linalg.inv(L)
            Qx = L_inv @ np.transpose(L_inv)  # N^-1 = (L L^t)^-1 = L^-1 (L^-1)^t
            dZ = np.linalg.solve(L, K)
            dX = np.linalg.solve(np.transpose(L), dZ)

        # R = N and K = Q^T @ B
        elif inversion_method == "qr":
            R_inv = np.linalg.inv(N)
            Qx = R_inv @ np.transpose(R_inv)
            dX = np.linalg.solve(N, K)

        else:
            raise ValueError("Uncorrect inversion method")

        if verbosity >= 1:
            print("         * Parameters update : ")
            print(dX)

        return dX, Qx / self.sig0 ** 2

    def inverse_unconstrained_stacked_neq(self, N, K, inversion_method="simple", verbosity=0, already_constrained=False):
        """
        Inverse stacked normal equations

        :param N: Unconstrained normal matrix
        :type N: Array
        :param K: Unconstrained right member
        :type K: Array
        :param inversion_method: inversion method to use for N
        :type inversion_method: str
        :param verbosity: level of verbosity (0, 1, 2), defaults to 0
        :type verbosity: int, optional
        :param already_constrained: either the system is already constrained, defaults to False
        :type already_constrained: bool, optional
        """

        X0 = np.array([self.init_parameters[p].value for p in self.init_parameters])
        X0 = X0.reshape(X0.size, 1)

        if not already_constrained:

            Nc, Kc = self._get_constraints_matrices(input_parameters=self.init_parameters)  # when estimation is finished, right end member is expressed from X0
            N += Nc
            K += Kc

            if verbosity >= 2:
                print("         * X0 : \n {}".format(X0))
                print("         * N : \n {}".format(N))
                print("         * Nc : \n {}".format(Nc))
                print("         * K : \n {}".format(K))
                print("         * Kc : \n {}".format(Kc))

        if inversion_method == "qr":
            inversion_method = "cholesky"

        dX, Qx = self.inverse_normal_equation(N, K, inversion_method)
        if verbosity >= 1:
            print("         * Parameters update : ")
            print(dX)

        self.update_params(X0 + dX, Qx)

        self.estimate_vector = X0 + dX
        self.estimate_covar = Qx

        self.normal_equation = N
        self.right_hand_member = K

        self.residuals = self.func_fit(except_rows=self.detected_outliers_idx)
        self.compute_statistics()


    """
    Exports
    """

    def to_minimizer_result(self):
        """
        Exports inversion results to :mod:lmfit minimizer output

        :return: exported results
        :rtype: lmfit.MinimizerResult
        """

        # for printing, set init value with real init value not update ones
        par_to_print = copy.deepcopy(self.parameters)
        for p in self.parameters:
            par_to_print[p].init_value = self.init_parameters[p].value

        r = super().to_minimizer_result()
        r.method = "lsq"

        return r


"""
Afterwards processing
"""


def stack_unconstrained_normal_equation(N, K, in_estimator, X0=None):
    """
    Stacks unconstrained normal equations from least-squares estimation results onto normal equation matrices

    :param N: input unconstrained normal equation matrix on which to stack results
    :type N: Array
    :param K: input unconstrained right member matrix on which to stack results
    :type K: Array
    :param in_estimator: unconstrained estimation results to stack
    :type in_estimator: EstimationModel
    :param X0: input parameters apriori vector if not similar to estimation results, defaults to None
    :type X0: Array, optional
    :returns:
            - **N** (Array) stacked normal equation matrix
            - **K** (Array) stacked right member
    """
    if X0 is not None:

        dX0 = in_estimator.init_vector.reshape((in_estimator.init_vector.size, 1)) - X0.reshape((X0.size, 1))
        dK0 = np.dot(in_estimator.normal_equation, dX0)

    else:

        dK0 = 0

    N += in_estimator.normal_equation
    K += in_estimator.right_hand_member + dK0

    return N, K


def only_keep_stacking_opts_from_lse_opts(estimation_opts):
    """
    Removes all non allowed options for stacking in estimation options

    :param estimation_opts: input estimation options
    :type estimation_opts: dict
    :return: stacking options
    :rtype: dict
    """
    stacking_opts = {}
    for opt in ["inversion_method", "verbose"]:
        if opt in estimation_opts.keys():
            stacking_opts[opt] = estimation_opts[opt]

    return stacking_opts
