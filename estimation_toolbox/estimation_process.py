import sys
from abc import abstractmethod
import numpy as np
from inspect import isfunction
import lmfit
from lmfit.minimizer import MinimizerResult
import copy
import pygmt
from scipy.stats import norm

# from estimation_toolbox.estimation_model import _InitializeParameterizedModel, model_constructor

# from typing import override

""" Some important constants (avoid editing these) """

CONSTRAINT_NAME = "parameter"
CONSTRAINT_VAL = "value"
CONSTRAINT_STD = "std"
CONSTRAINT_FUNC_ARGS = "function_args"

"""
****************************************************************************************
Estimation object = EstimationProcessMixin + EstimationModel + EvolutionModel (optional)
****************************************************************************************
"""

global ImplementedEstimation

def estimation_constructor(estimation_model, estimation_process, evolution_model=None):
    """
    Factory that allows to define Estimation class on the fly from EstimationModelMixin and EstimationProcessMixin

    :return: on the fly defined class
    :rtype: Estimation
    """
    
    if evolution_model is None:

        class ImplementedEstimation(estimation_model, estimation_process):

            def __init__(self, kwargs_estimation_model, kwargs_estimation_process):

                self.parent_estimation_model = estimation_model
                self.parent_estimation_process = estimation_process

                # kwargs = {**kwargs_estimation_model, **kwargs_estimation_process}
                # super().__init__(**kwargs)
                estimation_model.__init__(self, **kwargs_estimation_model)
                estimation_process.__init__(self, **kwargs_estimation_process)

            def __reduce__(self):
                """
                Overrides __reduce__ method for allowing pickling of classes created on the fly

                see : https://stackoverflow.com/questions/4647566/pickle-a-dynamically-parameterized-sub-class
                """

                # if '<locals>' in estimation_model.__qualname__:

                return (
                    _InitializeParameterizedEstimation(),
                    (self.parent_estimation_model, self.parent_estimation_process),
                    self.__dict__
                )

        return ImplementedEstimation

    else:

        class ImplementedEstimation(estimation_model, estimation_process, evolution_model):

            def __init__(self, kwargs_estimation_model, kwargs_estimation_process, kwargs_evolution_model):

                self.parent_estimation_model = estimation_model
                self.parent_estimation_process = estimation_process
                self.parent_evolution_model = evolution_model

                estimation_model.__init__(self, **kwargs_estimation_model)
                estimation_process.__init__(self, **kwargs_estimation_process)
                evolution_model.__init__(self, **kwargs_evolution_model)

            def __reduce__(self):
                """
                Overrides __reduce__ method for allowing pickling of classes created on the fly

                see : https://stackoverflow.com/questions/4647566/pickle-a-dynamically-parameterized-sub-class
                """

                # if '<locals>' in estimation_model.__qualname__:

                return (
                    _InitializeParameterizedEstimation(),
                    (self.parent_estimation_model, self.parent_estimation_process, self.parent_evolution_model),
                    self.__dict__
                )

        return ImplementedEstimation

class _InitializeParameterizedEstimation(object):
    """
    When called with the param value as the only argument, returns an
    un-initialized instance of the parameterized class. Subsequent __setstate__
    will be called by pickle.

    see : https://stackoverflow.com/questions/4647566/pickle-a-dynamically-parameterized-sub-class
    """

    def __call__(self, estimation_model, estimation_process, evolution_model=None):

        # make a simple object which has no complex __init__ (this one will do)
        obj = _InitializeParameterizedEstimation()
        obj.__class__ = estimation_constructor(estimation_model, estimation_process, evolution_model)

        return obj

class EstimationProcess:
    """
    Class that defines an estimation process (algorithm employed)
    """

    def __init__(self, *args, **kwargs):

        # this works because EstimationProcess is instantiated after EstimationModel

        self.estimate_vector = None
        self.estimate_covar = None
        self.residuals = None

        # Solution statistics
        self.v_chi2 = 0
        self.aic = 0
        self.bic = 0

        self.add_constraints(self._get_apriori_const_from_init_parameters())

    """
    Handling constraints
    """

    def add_constraints(self, const):
        """
        Adds constraints to the model

        :param const: input constraints
        :type const: list[dict]
        :return:
        """
        new_const = copy.deepcopy(self.constraints) + const
        self.constraints = new_const
        self.n_const = len(self.constraints)

    def remove_constraints(self):
        """
        Removes applied constraints

        :return:
        """
        self.constraints = []
        self.n_const = len(self.constraints)

    def _get_apriori_const_from_init_parameters(self):
        """
        Gets minimal constraints from parameters initialization

        :return: created minimal constraints
        :rtype: list[dict]
        """
        const = []
        for p in self.init_parameters:
            const.append({
                CONSTRAINT_NAME: p,
                CONSTRAINT_VAL: self.init_parameters[p].value,
                CONSTRAINT_STD: max(
                    np.abs(self.init_parameters[p].value - self.init_parameters[p].min),
                    np.abs(self.init_parameters[p].value - self.init_parameters[p].max)
                ) / 3
            })

        return const

    def _get_const_value(self, const, input_parameters):
        """
        Get constraint value

        :param const: input constraints
        :type const: dict
        :param input_parameters: Parameters to use for constraints definition, defaults to None
        :type input_parameters: lmfit.Parameters, optional
        :return: constraints value
        :rtype: float
        """
        if isfunction(const[CONSTRAINT_VAL]):
            return const[CONSTRAINT_VAL](input_parameters, *const[CONSTRAINT_FUNC_ARGS])
        else:
            return const[CONSTRAINT_VAL]

    def _get_const_jac(self, const, ref_param, input_parameters):
        """
        Returns the partial derivative of a constrained by a parameter

        :param const: constraint to derive
        :type const: dict
        :param ref_param: parameter name of the target jacobian column
        :type ref_param: str
        :param input_parameters: Parameters to use for constraints definition, defaults to None
        :type input_parameters: lmfit.Parameters, optional
        :return: jacobian cell value
        :rtype: float
        """
        # if an expression is given the jacobian value is computed
        if isfunction(const[CONSTRAINT_VAL]):

            if const[CONSTRAINT_NAME] == ref_param:
                return -1

            else:
                # add partial derivative of function for the target parameter
                bis = copy.deepcopy(input_parameters)
                bis[ref_param].set(value=bis[ref_param].value + self.delt_data)
                return (const[CONSTRAINT_VAL](bis, *const[CONSTRAINT_FUNC_ARGS]) - const[CONSTRAINT_VAL](
                    input_parameters, *const[CONSTRAINT_FUNC_ARGS])) / self.delt_data

        else:
            if const[CONSTRAINT_NAME] == ref_param:
                return 1
            else:
                return 0

    def _get_constraints_matrices(self, input_parameters=None):
        """
        Computes constraint matrices

        :param input_parameters: Parameters to use for constraints definition, defaults to None
        :type input_parameters: lmfit.Parameters, optional
        :return:
            - **Bc** (Array) residuals vector
            - **Ac** (Array) Jacobian matrix
            - **Wc** (Array) Variance of constraints
        """
        if input_parameters is None:
            input_parameters = self.parameters

        Bc = np.array([
            self._get_const_value(c, input_parameters) - input_parameters[c[CONSTRAINT_NAME]]
            for c in self.constraints
        ])
        Wc = np.array([
            c[CONSTRAINT_STD] for c in self.constraints
        ]) ** 2

        rows = []
        for c in self.constraints:
            cols = []
            for param in input_parameters:
                cols.append(self._get_const_jac(c, param, input_parameters))

            rows.append(cols)

        Ac = np.array(rows)

        return Bc.reshape(Bc.size, 1), Ac, Wc

    """
    Statistics
    """

    def degree_of_freedom(self):
        """
        Computes estimation degree of freedom

        :return: estimation degree of freedom
        :rtype: float
        """
        return self.n_obs + self.n_const - len(self.detected_outliers_idx) - self.n_par

    def compute_aic(self):
        """
        Computes AIC

        :return:
        """
        self.aic = (self.n_obs - len(self.detected_outliers_idx)) * np.log(
            self.v_chi2 / (self.n_obs - len(self.detected_outliers_idx))) + 2 * self.n_par

    def compute_bic(self):
        """
        Computes BIC

        :return:
        """
        self.bic = (self.n_obs - len(self.detected_outliers_idx)) * np.log(
            self.v_chi2 / (self.n_obs - len(self.detected_outliers_idx))) + np.log(
            (self.n_obs - len(self.detected_outliers_idx))) * self.n_par

    def compute_chi2(self):
        """
        Computes χ²

        :return:
        """
        self.v_chi2 = np.sum(
            (self.residuals ** 2))

    def compute_statistics(self):

        self.compute_chi2()
        self.compute_aic()
        self.compute_bic()
        self.compute_rmse()
        self.compute_wrmse()

    def compute_rmse(self):
        self.rmse = np.sqrt(np.sum(self.residuals ** 2) / self.n_obs)

    def compute_wrmse(self):
        var = self.get_observations_variance(except_rows=self.detected_outliers_idx)
        self.wrmse = np.sqrt(np.sum((self.residuals ** 2) / var) / (1 / np.sum(var)))

    def test_parameter_significance(self, param, alpha=0.05):
        """
        Test on estimation parameters significance

        :param alpha: confidence interval, defaults to 0.05
        :type alpha: float, optional
        :param param: parameter to test
        :type param: lmfit.Parameter
        :return: test is passed
        :rtype: bool
        """
        return abs(param.value - param.init_value) / (self.v_chi2 * param.stderr) <= scipy.stats.t.ppf(1 - alpha / 2,
                                                                                                       self.degree_of_freedom())

    def test_chi2(self, alpha=0.05):
        """
        Test on estimation a priori χ²

        :param alpha: confidence interval, defaults to 0.05
        :type alpha: float, optional
        :return: test is passed
        :rtype: bool
        """
        return scipy.stats.chi2.ppf(alpha / 2, self.degree_of_freedom()) < self.v_chi2 < scipy.stats.chi2.ppf(
            1 - alpha / 2, self.degree_of_freedom())

    """
    Exports
    """

    def pretty_print(self):
        """
        Pretty print using lmfit.fit_report

        :return:
        """
        print(lmfit.fit_report(self.to_minimizer_result()))
        print("RMSE = {}".format(self.rmse))
        print("WRMSE = {}".format(self.wrmse))

    def to_sinex(self):

        # TODO
        # https://www.iers.org/SharedDocs/Publikationen/EN/IERS/Documents/ac/sinex/sinex_v202_pdf.pdf?__blob=publicationFile&v=2
        pass

    """
    Utility functions
    """

    def to_minimizer_result(self):

        r = MinimizerResult(params=self.parameters, var_names=[p for p in self.parameters],
                            covar=np.diag([self.parameters[p].stderr for p in self.parameters]),
                            nvarys=self.n_par, ndata=(self.n_obs - len(self.detected_outliers_idx)),
                            nfree=self.degree_of_freedom(),
                            residual=self.residuals, chisqr=self.v_chi2, redchi=self.v_chi2 / self.degree_of_freedom(),
                            aic=self.aic, bic=self.bic
                            )
        r.errorbars = True
        return r


"""
Plots
"""


def plot_residuals_histogram(in_figure, residuals,
                             pygmt_conf=None, fig_size=None,
                             x_bounds=None, y_bounds=None,
                             x_ticks=None, y_ticks=None,
                             x_label=None, y_label=None, bin_width=10, text_y_space=0.5, distribution=True,
                             ):
    """
    Plots histogram of residuals

    :param in_figure: input figure
    :type in_figure: pygmt.Figure
    :param residuals: residuals to plot
    :type residuals: Array
    :param pygmt_conf: pygmt configurations, defaults to None
    :type pygmt_conf: dict, optional
    :param fig_size: size of figure, defaults to None
    :type fig_size: str, optional
    :param x_bounds: bounds of x-axis, defaults to None
    :type x_bounds: tuple, optional
    :param y_bounds: bounds of y-axis, defaults to None
    :type y_bounds: tuple, optional
    :param x_ticks: ticks of x-axis, defaults to None
    :type x_ticks: list[str], optional
    :param y_ticks: ticks of y-axis, defaults to None
    :type y_ticks: list[str], optional
    :param x_label: label of x-axis, defaults to None
    :type x_label: str, optional
    :param y_label: label of y-axis, defaults to None
    :type y_label: str, optional
    :param bin_width: width of histogram bins, defaults to 10
    :type bin_width: int, optional
    :param text_y_space: y-space between text annotations in cm, defaults to 0.5
    :type text_y_space: float, optional
    :param distribution: whether to plot distribution, defaults to True
    :type distribution: bool, optional
    :return: histogram figure
    :rtype: pygmt.Figure
    """

    if fig_size is None:
        fig_size = "10c"
    if pygmt_conf is None:
        pygmt_conf = {}
    if x_ticks is None:
        x_ticks = ["xafg"]
    if y_ticks is None:
        y_ticks = ["yafg"]
    if x_label is None:
        x_label = []
    else:
        x_label = ["x+l{}".format(x_label)]
    if y_label is None:
        y_label = []
    else:
        y_label = ["y+l{}".format(y_label)]
    if x_bounds is None:
        x_bounds = (np.min(residuals), np.max(residuals))
    if y_bounds is None:
        y_bounds = (0, 50)

    with pygmt.config(
            **pygmt_conf
    ):

        in_figure.histogram(
            region=[*x_bounds, *y_bounds],
            projection="X" + fig_size,
            frame=["EStl"] + y_label + x_ticks + y_ticks + x_label,
            data=residuals,
            series=bin_width,
            fill="red3",
            histtype=1,
            distribution="0+p1p,blue,-" if distribution else None
        )

        mu, sigma = norm.fit(residuals)
        # in_figure.plot(
        #     x=np.linspace(*x_bounds, 200),
        #     y=norm.pdf(np.linspace(*x_bounds, 200), mu, sigma),
        #     pen="1p,blue,-"
        # )

        in_figure.text(text="N = {}".format(residuals.size),
                       position="TR", justify="TR", offset="-0.1c")

        in_figure.text(text="@~m@~ = {:.2f}".format(mu),
                       position="TR", justify="TR", offset="-0.1c/-{}c".format(text_y_space + 0.1))

        in_figure.text(text="@~s@~ = {:.2f}".format(sigma),
                       position="TR", justify="TR", offset="-0.1c/-{}c".format(text_y_space * 2 + 0.1))

        return in_figure


def plot_residuals_frequency_analysis(in_fig, in_x, in_y, frequencies,
                                      pygmt_conf=None, fig_size=None,
                                      x_ticks=None, y_ticks=None,
                                      x_label=None, y_label=None,
                                      x_log=False
                                      ):
    """
    Plots frequency analysis of residuals

    :param in_fig: input figure
    :type in_fig: pygmt.Figure
    :param in_x: residuals x-values
    :type in_x: Array
    :param in_y: residuals y-values
    :type in_y: Array
    :param frequencies: frequencies at which to do frequency analysis
    :type frequencies: Array
    :param pygmt_conf: pygmt configurations, defaults to None
    :type pygmt_conf: dict, optional
    :param fig_size: size of figure, defaults to None
    :type fig_size: str, optional
    :param x_ticks: ticks of x-axis, defaults to None
    :type x_ticks: list[str], optional
    :param y_ticks: ticks of y-axis, defaults to None
    :type y_ticks: list[str], optional
    :param x_label: label of x-axis, defaults to None
    :type x_label: str, optional
    :param y_label: label of y-axis, defaults to None
    :type y_label: str, optional
    :param x_log: either to set x-axis in log scale, defaults to False
    :type x_log: bool, optional
    :return: histogram figure
    :rtype: pygmt.Figure
    """

    if fig_size is None:
        fig_size = "10c"
    if pygmt_conf is None:
        pygmt_conf = {}
    if x_ticks is None:
        x_ticks = ["xafg"]
    if y_ticks is None:
        y_ticks = ["yafg"]
    if x_label is None:
        x_label = []
    else:
        x_label = ["x+l{}".format(x_label)]
    if y_label is None:
        y_label = []
    else:
        y_label = ["y+l{}".format(y_label)]

    with pygmt.config(**pygmt_conf):

        proj = "X" + fig_size
        if x_log:
            proj = proj.split("/")[0] + "l/" + proj.split("/")[1]

        in_fig.basemap(
            region=[np.min(frequencies), np.max(frequencies), -0.1, 1.1],
            projection=proj,
            frame=["EStl"] + y_label + x_ticks + y_ticks + x_label,
        )

        pgr = lombscargle(in_x, in_y, frequencies, normalize=True)
        in_fig.plot(
            x=frequencies,
            y=pgr,
            pen="1p,black"
        )

    return in_fig


def plot_model_against_observations(in_fig, in_x, in_y, in_y_model,
                                    pygmt_conf=None, fig_size=None,
                                    x_bounds=None, y_bounds=None,
                                    x_ticks=None, y_ticks=None,
                                    x_label=None, y_label=None,
                                    ):
    """
    Plots observation data along with model

    :param in_fig: input figure
    :type in_fig: pygmt.Figure
    :param in_x: x-values
    :type in_x: Array
    :param in_y: observation y-values
    :type in_y: Array
    :param in_y_model: model y-values
    :type in_y_model: Array
    :param pygmt_conf: pygmt configurations, defaults to None
    :type pygmt_conf: dict, optional
    :param x_bounds: bounds of x-axis, defaults to None
    :type x_bounds: tuple, optional
    :param y_bounds: bounds of y-axis, defaults to None
    :type y_bounds: tuple, optional
    :param fig_size: size of figure, defaults to None
    :type fig_size: str, optional
    :param x_ticks: ticks of x-axis, defaults to None
    :type x_ticks: list[str], optional
    :param y_ticks: ticks of y-axis, defaults to None
    :type y_ticks: list[str], optional
    :param x_label: label of x-axis, defaults to None
    :type x_label: str, optional
    :param y_label: label of y-axis, defaults to None
    :type y_label: str, optional
    :return: histogram figure
    :rtype: pygmt.Figure
    """

    if fig_size is None:
        fig_size = "10c"
    if pygmt_conf is None:
        pygmt_conf = {}
    if x_ticks is None:
        x_ticks = ["xafg"]
    if y_ticks is None:
        y_ticks = ["yafg"]
    if x_bounds is None:
        x_bounds = (np.min(in_x), np.max(in_x))
    if y_bounds is None:
        y_bounds = (np.min(in_y), np.max(in_y))

    with pygmt.config(**pygmt_conf):

        in_fig.basemap(
            region=[*x_bounds, *y_bounds],
            projection="X" + fig_size,
            frame=["EStl"] + y_label + x_ticks + y_ticks + x_label,
        )

        in_fig.plot(x=in_x, y=in_y, label="Data", style="c0.05c", fill="black")
        in_fig.plot(x=in_x, y=in_y_model, label="Model", style="c0.05c", fill="red")

        return in_fig
