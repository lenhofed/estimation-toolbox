import copy
import sys
import numpy as np
import pandas as pd
from lmfit.minimizer import MinimizerResult
import lmfit
from abc import abstractmethod
# from typing import override

from estimation_toolbox.estimation_process import EstimationProcess, estimation_constructor
# from estimation_toolbox.estimation_model import model_constructor



class ExtendedKalmanEpoch(EstimationProcess):
    """
    Estimation process for a given EKF state (epoch)
    """

    def __init__(self, t_ref, *args, **kwargs):
        """

        :param t_ref: reference time of Kalman epoch
        """
        
        self.t_ref = t_ref
        self.X_pred = None
        self.Q_pred = None
        self.F = None
        self.X_up = None
        self.Q_up = None
        self.X_smooth = None
        self.Q_smooth = None
        
        super().__init__(*args, **kwargs)

        

    def observation_covar(self, *args, **kwargs):
        """
        Gets matrix of observation variance-covariance
        """
        return np.diag(self.get_observations_variance(*args, **kwargs))

    def _constrain_matrices(self, y_pred, H, W):
        """
        Applies constraints to observation matrices
        """

        if len(self.constraints) != 0:
            yc, Hc, Wc = self._get_constraints_matrices()
            Wc = np.diag(Wc)

            y_pred = np.concatenate((y_pred, yc))
            H = np.concatenate((H, Hc))
            W = np.concatenate((
                np.concatenate((
                    W, np.zeros((W.shape[0], Wc.shape[1]))
                ), axis=1),
                np.concatenate((
                    np.zeros((Wc.shape[0], W.shape[1])), Wc
                ), axis=1)
            ))

        return y_pred, H, W

    """
    Kalman filter estimation
    """

    def predict(self, previous_epoch):
        """
        Predicts epoch state from previous state and evolution model

        :param previous_epoch: previous state
        :type previous_epoch: ExtendedKalmanEpoch
        """
        # update current parameters
        self.update_params(previous_epoch.X_up, previous_epoch.Q_up)

        # predicted parameters vector
        self.X_pred = self.prediction_model(previous_epoch.t_ref)

        # predicted parameters covariance
        W = self.prediction_covar(previous_epoch.t_ref)
        self.F = self.prediction_jacobian(previous_epoch.t_ref)

        self.Q_pred = np.dot(
            self.F,
            np.dot(previous_epoch.Q_up, np.transpose(self.F))
        ) + W

        # update current parameters
        self.update_params(self.X_pred, self.Q_pred)


    def update(self):
        """
        Updates epoch state from observations and observation model
        """
        # update current parameters
        self.update_params(self.X_pred, self.Q_pred)

        # all data considered as outliers, then skip
        if self.get_observations(except_rows=self.detected_outliers_idx).shape[0] == 0:

            self.X_up = self.X_pred
            self.Q_up = self.Q_pred

        # run data assimilation
        else:

            # innovation matrices
            y_pred = self.func_fit(except_rows=self.detected_outliers_idx)
            y_pred = y_pred.reshape(y_pred.size, 1)

            H = self.jacobian(except_rows=self.detected_outliers_idx)

            Z = self.observation_covar(except_rows=self.detected_outliers_idx)

            # append constraints
            y_pred, H, Z = self._constrain_matrices(y_pred, H, Z)

            # innovation covariance
            C = np.dot(
                H, np.dot(self.Q_pred, np.transpose(H))
            ) + Z

            # Kalman gain
            K = np.dot(
                self.Q_pred,
                np.dot(
                    np.transpose(H),
                    np.linalg.inv(C)
                )
            )

            # updated parameters vector
            self.X_up = self.X_pred + np.dot(K, y_pred)

            # updated covariance of parameters
            self.Q_up = np.dot(
                np.eye(self.X_up.shape[0]) - np.dot(K, H),
                self.Q_pred
            )

        # update current parameters
        self.update_params(self.X_up, self.Q_up)

    """
    Backward smoother 
    """

    def rts_smoother(self, next_state):
        """
        Smooth epoch state from all next states and evolution model

        """
        # smoother gain
        C = np.dot(
            self.Q_up,
            np.dot(
                np.transpose(next_state.F),
                np.linalg.inv(next_state.Q_pred)
            )
        )

        # smoothed parameters vector
        self.X_smooth = self.X_up + np.dot(
            C,
            next_state.X_smooth - next_state.X_pred
        )

        # smooth covariance of parameters
        self.Q_smooth = self.Q_up + np.dot(
            C, np.dot(
                next_state.Q_smooth - next_state.Q_pred,
                np.transpose(C)
            )
        )

        # update current parameters
        self.update_params(self.X_smooth, self.Q_smooth)

    def select_solution(self, which_solution):
        """
        Activates given solution as parameters vector

        :param which_solution: parameters solution to activate
        :type which_solution: str
        :returns:
            - **X** (Array) parameters vector of given solution
            - **Q** (Array) covariance of given solution
        """
        if which_solution == "backward" and self.X_smooth is not None:
            return self.X_smooth, self.Q_smooth

        elif which_solution == "forward":
            return self.X_up, self.Q_up

        elif which_solution == "predicted":
            return self.X_pred, self.Q_pred

        elif which_solution == "initial":
            return self.initialize()

        else:
            raise ValueError()

    def initialize(self):
        """
        Gets vector and covariance of initial parameters

        :returns:
            - **X** (Array) parameters vector of initial solution
            - **Q** (Array) covariance of initial solution
        """
        X0 = np.array([self.init_parameters[p].value for p in self.init_parameters])
        X0 = X0.reshape(X0.size, 1)
        Q0 = np.diag(
            [max(
                np.abs(self.init_parameters[p].value - self.init_parameters[p].min),
                np.abs(self.init_parameters[p].value - self.init_parameters[p].max)
            ) for p in self.init_parameters]
        ) ** 2

        return X0, Q0

    def compute_residuals(self, *args, which_solution="forward", **kwargs):
        """
        Computes epoch residuals

        :param which_solution: which solution to compute residuals for, defaults to "forward"
        :type which_solution: str, optional
        :return: residuals
        :rtype: Array
        """
        self.update_params(*self.select_solution(which_solution))

        return self.func_fit(*args, **kwargs)

    def get_model_data(self, *args, which_solution="forward", **kwargs):
        """
        Gets epoch modeled data

        :param which_solution: which solution to get modeled data for, defaults to "forward"
        :type which_solution: str, optional
        :return: modeled data
        :rtype: Array
        """
        self.update_params(*self.select_solution(which_solution))

        return self.get_model(*args, **kwargs)

    def to_minimizer_result(self):
        """
        Exports inversion results to :mod:lmfit minimizer output

        :return: exported results
        :rtype: lmfit.MinimizerResult
        """
        self.residuals = self.func_fit(except_rows=self.detected_outliers_idx)
        self.compute_chi2()
        self.compute_aic()
        self.compute_bic()

        super().to_minimizer_result()

        r.errorbars = True
        r.method = "kalman"
        return r

class SquareRootFilterEpoch(ExtendedKalmanEpoch):
    """
    Estimation process for a given SRIF state (epoch)
    """

    def __init__(self, t_ref, *args, **kwargs):
        
        self.Q_pred_sqr = 0
        self.Q_up_sqr = 0
        
        super().__init__(t_ref, *args, **kwargs)
        

    # @override
    def predict(self, previous_epoch):
        """

        :param previous_epoch: previous state
        :type previous_epoch: SquareRootFilterEpoch
        """
        # update current parameters
        self.update_params(previous_epoch.X_up, previous_epoch.Q_up)

        # predicted parameters vector
        self.X_pred = self.prediction_model(previous_epoch.t_ref)

        # predicted parameters covariance
        Rw = np.transpose(np.linalg.cholesky(self.prediction_covar(previous_epoch.t_ref)))  # triangular sup
        self.F = self.prediction_jacobian(previous_epoch.t_ref)

        # see https://www.youtube.com/watch?v=XefqNr9dj7I
        At = np.concatenate((
            np.transpose(np.dot(self.F, previous_epoch.Q_up_sqr)),
            np.transpose(Rw)
        ))

        _, Rt = np.linalg.qr(At)

        self.Q_pred_sqr = np.transpose(Rt)
        self.Q_pred = np.dot(self.Q_pred_sqr, Rt)

        # update current parameters
        self.update_params(self.X_pred, self.Q_pred)

    # @override
    def update(self, *args, **kwargs):
        # update current parameters
        self.update_params(self.X_pred, self.Q_pred)

        # all data considered as outliers, then skip
        if self.get_observations(except_rows=self.detected_outliers_idx).shape[0] == 0:

            self.X_up = self.X_pred
            self.Q_up = self.Q_pred

        # run data assimilation
        else:
            try:
                # innovation matrices
                y_pred = self.func_fit(except_rows=self.detected_outliers_idx)
                y_pred = y_pred.reshape(y_pred.size, 1)

                H = self.jacobian(except_rows=self.detected_outliers_idx)

                Z = self.observation_covar(except_rows=self.detected_outliers_idx)

                # append constraints
                y_pred, H, Z = self._constrain_matrices(y_pred, H, Z)

                n, p = y_pred.shape[0], self.X_pred.shape[0]

                # First method
                # see https://gitlab.com/lenhofed/stage_kalman/-/blob/start_branch/prototype/kalman/kalmanFilter.py?ref_type=heads
                At = np.zeros((n + p, n + p))
                # left upper part
                At[:n, :n] = np.linalg.cholesky(Z).T
                # left lower part
                At[n:, :n] = np.dot(self.Q_pred_sqr.T, H.T)
                # right lower part
                At[n:, n:] = self.Q_pred_sqr.T

                _, Rt = np.linalg.qr(At)
                self.Q_up_sqr = Rt[n:, n:].T

                # Kalman gain
                K = np.dot(
                    Rt[:n, n:].T,
                    np.linalg.inv(Rt[:n, :n].T)
                )

                # Second method
                # see https://www.youtube.com/watch?v=XefqNr9dj7I
                # A = np.dot(H, self.Q_pred_sqr)
                # At = np.transpose(A)
                # B_inv = np.linalg.inv(
                #     np.dot(A, At) + Z
                # )
                #
                # # Kalman gain
                # K = np.dot(self.Q_pred_sqr, np.dot(At, B_inv))
                #
                # # P = (I - KH)P = R (I - At Binv A ) Rt
                # B2 = np.eye(len(self.parameters)) - np.dot(
                #     At, np.dot(B_inv, A)
                # )
                #
                # # P = RC CtRt
                # C = np.transpose(np.linalg.cholesky(B2))
                # self.Q_up_sqr = np.dot(self.Q_pred_sqr, C)

                # updated parameters vector
                self.X_up = self.X_pred + np.dot(K, y_pred)

                # updated covariance of parameters
                self.Q_up = np.dot(self.Q_up_sqr, np.transpose(self.Q_up_sqr))

            except np.linalg.LinAlgError:
                # in case solution can't be computed for this epoch
                print("Solution could not be computed for this epoch")
                self.X_up = self.X_pred
                self.Q_up = self.Q_pred

        # update current parameters
        self.update_params(self.X_up, self.Q_up)

    # @override
    def to_minimizer_result(self):
        """
        Exports inversion results to :mod:lmfit minimizer output

        :return: exported results
        :rtype: lmfit.MinimizerResult
        """
        r = super().to_minimizer_result()
        r.method = "srif"
        return r


class KalmanFilter:
    """
    A Kalman Filter is a collection of multiple Estimation(EstimationModel, ExtendedKalmanEpoch) objects
    """

    # TODO : add some methods

    def __init__(self,
                 parameters, x_data_per_epoch, y_data_per_epoch, t_ref_per_epoch,
                 data_model, evolution_model, estimation_process,
                 data_model_kwargs=None, evo_model_kwargs=None, estimation_process_kwargs=None
                 ):
        """
        Creates KalmanFilter

        :param est_process: Filter process to use
        :type est_process: ExtendedKalmanEpoch
        :param est_model: estimation model to use
        :param parameters: parameters to estimate
        :type parameters: lmfit.Parameters
        :param x_data_per_epoch: x-axis data per epoch
        :type x_data_per_epoch: list[Array]
        :param y_data_per_epoch: y-axis data per epoch
        :type y_data_per_epoch: list[Array]
        :param t_ref_per_epoch: time of reference per epoch
        :type t_ref_per_epoch: list[datetime.datetime]
        :param process_kwargs: kwargs for Kalman process, defaults to None
        :type process_kwargs: dict, optional
        :param process_kwargs: kwargs for estimation model, defaults to None
        :type process_kwargs: dict, optional
        """

        if data_model_kwargs is None:
            data_model_kwargs = {}
        if estimation_process_kwargs is None:
            estimation_process_kwargs = {}
        if evo_model_kwargs is None:
            evo_model_kwargs = {}

        self.parameters = parameters
        self.state_per_epoch = []
        self.t_ref_per_epoch = t_ref_per_epoch
        self.init_parameters = parameters
        self.n_par = len(parameters)
        self.n_obs = pd.concat(y_data_per_epoch).shape[0]

        self.constraints = []
        if "constraints" in data_model_kwargs and data_model_kwargs["constraints"] is not None:
            self.constraints = data_model_kwargs["constraints"]
        self.n_const = len(self.constraints)

        self.detected_outliers_idx = []

        self.residuals = None
        self.aic = 0
        self.bic = 0
        self.v_chi2 = 0
        self.rmse = 0
        self.wrmse = 0

        c = estimation_constructor(data_model, estimation_process, evolution_model)

        for i in range(len(self.t_ref_per_epoch)):

            self.state_per_epoch.append(
                c(
                    kwargs_estimation_model={
                            "parameters": parameters,
                            "x_data": x_data_per_epoch[i],
                            "y_data": y_data_per_epoch[i],
                            **data_model_kwargs
                            },
                    kwargs_estimation_process={
                        "t_ref": t_ref_per_epoch[i],
                        **estimation_process_kwargs
                    },
                    kwargs_evolution_model={
                        **evo_model_kwargs,
                    }
                )

            )

    def run_forward(self, verbosity=0, compute_res=False):
        """
        Runs forward Kalman processing

        :param verbosity: verbosity level, defaults to 0
        :type verbosity: int, optional
        """
        print(" * Running forward Kalman filter ({} epochs)".format(len(self.state_per_epoch)))

        n = len(self.t_ref_per_epoch)
        for i in range(n):

            if verbosity > 0:
                print("     # {} / {}".format(i + 1, n))

            # PREDICTION STEP

            # if first iteration, no prediction and directly update with data
            if i == 0:

                X0, Q0 = self.state_per_epoch[i].initialize()
                self.state_per_epoch[i].X_pred = X0
                self.state_per_epoch[i].Q_pred = Q0
                if isinstance(self.state_per_epoch[i], SquareRootFilterEpoch):
                    self.state_per_epoch[i].Q_pred_sqr = np.transpose(np.linalg.cholesky(Q0))

                if verbosity > 1:
                    print("     --> Initialization : ")
                    print(self.state_per_epoch[i].X_pred)
                    print(self.state_per_epoch[i].Q_pred)

            # run prediction
            else:
                self.state_per_epoch[i].predict(
                    self.state_per_epoch[i - 1]
                )
                if verbosity > 1:
                    print("     --> Prediction result : ")
                    print(self.state_per_epoch[i].X_pred)
                    print(self.state_per_epoch[i].Q_pred)

            # ESTMATION STEP

            # if no data available for epoch, skip update
            if self.state_per_epoch[i].x_data is None or self.state_per_epoch[i].y_data is None:

                self.state_per_epoch[i].X_up = self.state_per_epoch[i].X_pred
                self.state_per_epoch[i].Q_up = self.state_per_epoch[i].Q_pred

            # run update
            else:
                self.state_per_epoch[i].update()


            if verbosity > 1:
                print("     --> Estimation result : ")
                print(self.state_per_epoch[i].X_up)
                print(self.state_per_epoch[i].Q_up)

        if compute_res:
            self.residuals = self.func_fit(except_outliers=True, which_solution="forward")
            self.compute_statistics()

    def run_backward(self, verbosity=0):
        """
        Runs backward Kalman processing

        :param verbosity: verbosity level, defaults to 0
        :type verbosity: int, optional
        """
        print(" * Running backward Kalman filter ({} epochs)".format(len(self.state_per_epoch)))

        n = len(self.t_ref_per_epoch) - 1
        for i in range(n, -1, -1):

            if verbosity > 0:
                print("     # {} / {}".format(n - i + 1, n + 1))

            # last one not smoothed
            if i == n:
                self.state_per_epoch[n].X_smooth = self.state_per_epoch[n].X_up
                self.state_per_epoch[n].Q_smooth = self.state_per_epoch[n].Q_up

            else:

                # if no update was done on forward pass,
                # smoother gain is to high and the solution is completely false
                if (self.state_per_epoch[i].Q_up == self.state_per_epoch[i].Q_pred).all():

                    self.state_per_epoch[i].X_smooth = self.state_per_epoch[i+1].X_smooth
                    self.state_per_epoch[i].Q_smooth = self.state_per_epoch[i + 1].Q_smooth

                else:
                    self.state_per_epoch[i].rts_smoother(
                        self.state_per_epoch[i+1]
                    )

            if verbosity > 1:
                print("     --> Smoothing result : ")
                print(self.state_per_epoch[i].X_smooth)
                print(self.state_per_epoch[i].Q_smooth)

        self.residuals = self.func_fit(except_outliers=True, which_solution="backward")
        self.compute_statistics()

    """
    Data Manipulation over the whole filter
    """

    def get_x_values(self, *args, except_outliers=False, **kwargs):
        """
        Gets x-axis values for all epochs
        """
        x_data = []
        n = len(self.t_ref_per_epoch)
        except_rows = []
        if "except_rows" in kwargs:
            except_rows = copy.copy(kwargs["except_rows"])

        for i in range(n):
            if except_outliers:
                kwargs["except_rows"] = self.state_per_epoch[i].detected_outliers_idx + except_rows

            x_data.append(self.state_per_epoch[i].get_x_values(*args, **kwargs))

        return np.concatenate(x_data)

    def get_observations(self, *args, except_outliers=False, **kwargs):
        """
        Gets observations for all epochs
        """
        x_data = []
        n = len(self.t_ref_per_epoch)
        for i in range(n):

            if except_outliers:
                kwargs["except_rows"] = self.state_per_epoch[i].detected_outliers_idx

            x_data.append(self.state_per_epoch[i].get_observations(*args, **kwargs))

        return np.concatenate(x_data)

    def get_observations_variance(self, *args, except_outliers=False, **kwargs):
        """
        Gets observations for all epochs
        """
        x_data = []
        n = len(self.t_ref_per_epoch)
        for i in range(n):

            if except_outliers:
                kwargs["except_rows"] = self.state_per_epoch[i].detected_outliers_idx

            x_data.append(self.state_per_epoch[i].get_observations_variance(*args, **kwargs))

        return np.concatenate(x_data)

    def get_model(self, *args, which_solution="forward", except_outliers=False, **kwargs):
        """
        Gets modeled y-axis values for all epochs

        :param which_solution: which solution to get modeled data for, defaults to "forward"
        :type which_solution: str, optional
        :return: modeled y-axis values for all epochs
        :rtype: Array
        """
        model_data = []
        n = len(self.t_ref_per_epoch)
        for i in range(n):

            if except_outliers:
                kwargs["except_rows"] = self.state_per_epoch[i].detected_outliers_idx

            model_data.append(self.state_per_epoch[i].get_model_data(*args, which_solution=which_solution, **kwargs))

        return np.concatenate(model_data)

    def func_fit(self, *args, which_solution="forward", except_outliers=False, **kwargs):
        """
        Computes for all epochs

        :param which_solution: which solution to compute residuals for, defaults to "forward"
        :type which_solution: str, optional
        :return: residuals for all epochs
        :rtype: Array
        """
        res = []
        n = len(self.t_ref_per_epoch)

        for i in range(n):

            if except_outliers:
                kwargs["except_rows"] = self.state_per_epoch[i].detected_outliers_idx

            if not self.state_per_epoch[i].get_observations(*args,
                    # except_rows=self.state_per_epoch[i].detected_outliers_idx,
                **kwargs
            ).shape[0] == 0:
                res.append(self.state_per_epoch[i].compute_residuals(*args, which_solution=which_solution, **kwargs))

        return np.concatenate(res)

    def get_parameter_time_series(self, parameter, which_solution="forward"):
        """
        Returns time series of estimated parameter

        :param parameter: parameter's name to retrieve
        :type parameter: str
        :param which_solution: which solution to compute time series for, defaults to "forward"
        :type which_solution: str, optional
        :return: time series of estimated parameter
        :rtype: pd.DataFrame
        """
        r = pd.DataFrame(columns=["t", "v", "std"])
        n = len(self.t_ref_per_epoch)
        for i in range(n):
            self.state_per_epoch[i].update_params(*self.state_per_epoch[i].select_solution(which_solution))

            r.loc[i] = [
                self.t_ref_per_epoch[i],
                self.state_per_epoch[i].parameters[parameter].value,
                self.state_per_epoch[i].parameters[parameter].stderr
            ]

        return r

    """
    Utility functions
    """

    def set_outliers_to_all_epochs(self):
        """
        Sets outliers for each epoch based on all epochs detected outliers
        """
        n = len(self.t_ref_per_epoch)
        idx_start = 0
        for i in range(n):

            # affect outliers to kalman epoch
            self.state_per_epoch[i].detected_outliers_idx = [
                idx - idx_start for idx in self.detected_outliers_idx if
                0 <= idx - idx_start < self.state_per_epoch[i].y_data.shape[0]
            ]
            idx_start += self.state_per_epoch[i].y_data.shape[0]

    def detect_outliers(self, detection_method, verbosity, which_solution="initial"):
        """
        Detects outliers in residuals  on all epochs using given detection method

        :param detection_method: outliers detection method to use
        :type detection_method: method
        :param verbosity: level of verbosity (0, 1, 2)
        :type verbosity: int
        """
        # detect outliers on all data
        out_idx = detection_method(self.func_fit(which_solution))

        print("         * Detecting {} outliers with method {}".format(
            len(out_idx), detection_method
        ))
        self.detected_outliers_idx = out_idx

        if verbosity == 2:
            print("         * Outliers indices : \n {}".format(self.detected_outliers_idx))

    def pretty_print(self, which_solution="forward", which_epoch=None):

        if which_epoch is None:
            print(lmfit.fit_report(self.to_minimizer_result()))
            print("RMSE = {}".format(self.rmse))
            print("WRMSE = {}".format(self.wrmse))

        elif isinstance(which_epoch, int):
            self.state_per_epoch[which_epoch].update_params(
                *self.state_per_epoch[which_epoch].select_solution(which_solution)
            )
            self.state_per_epoch[which_epoch].pretty_print()

        else:
            raise ValueError()

    def to_minimizer_result(self):

        r = MinimizerResult(
            params=self.parameters, var_names=[p for p in self.parameters],
                        covar=np.diag([self.parameters[p].stderr for p in self.parameters]),
                            nvarys=self.n_par, ndata=(self.n_obs - len(self.detected_outliers_idx)),
                            nfree=self.degree_of_freedom(),
                            residual=self.residuals, chisqr=self.v_chi2, redchi=self.v_chi2 / self.degree_of_freedom(),
                            aic=self.aic, bic=self.bic
                            )
        r.errorbars = True

        r.method = "kalman"
        return r

    def compute_statistics(self):

        self.compute_chi2()
        self.compute_aic()
        self.compute_bic()
        self.compute_rmse()
        self.compute_wrmse()

    def compute_rmse(self):
        self.rmse = np.sqrt(np.sum(self.residuals ** 2) / self.n_obs)

    def compute_wrmse(self):
        var = self.get_observations_variance(except_outliers=True)
        self.wrmse = np.sqrt(np.sum((self.residuals ** 2) / var) / (1 / np.sum(var)))


    def degree_of_freedom(self):
        """
        Computes estimation degree of freedom

        :return: estimation degree of freedom
        :rtype: float
        """
        return self.n_obs + self.n_const - len(self.detected_outliers_idx) - self.n_par

    def compute_aic(self):
        """
        Computes AIC

        :return:
        """
        self.aic = (self.n_obs - len(self.detected_outliers_idx)) * np.log(
            self.v_chi2 / (self.n_obs - len(self.detected_outliers_idx))) + 2 * self.n_par

    def compute_bic(self):
        """
        Computes BIC

        :return:
        """
        self.bic = (self.n_obs - len(self.detected_outliers_idx)) * np.log(
            self.v_chi2 / (self.n_obs - len(self.detected_outliers_idx))) + np.log(
            (self.n_obs - len(self.detected_outliers_idx))) * self.n_par

    def compute_chi2(self):
        """
        Computes χ²

        :return:
        """
        self.v_chi2 = np.sum(self.residuals ** 2)


