import copy
import sys
from abc import abstractmethod
from inspect import isfunction
# from typing import override

import lmfit
import numpy as np
import pygmt
from scipy.stats import norm
from scipy.signal import lombscargle

"""
Abstract classes
"""

class EvolutionModel:
    """
    Class that defines the state model evolution
    """

    def __init__(self, *args, delt=1e-6, **kwargs):
        """

        :param delt: delta to be used for jacobian computation, defaults to 1e-6
        :type delt: float, optional
        """
        self.delt_evo = delt

    @abstractmethod
    def prediction_model(self, *args, **kwargs):
        """
        Model of prediction from previous to current state

        :return: prediction model data
        :rtype: Array
        """
        pass

    def prediction_jacobian(self, *args, **kwargs):
        """
        Matrix of partial derivatives of the prediction model from previous to current state

        :return: prediction jacobian
        :rtype: Array
        """
        J0 = self.prediction_model(*args, **kwargs)
        cols = []
        for p in self.parameters:
            # adds delta to parameter
            self.parameters[p].value += self.delt_evo

            Jp = self.prediction_model(*args, **kwargs)
            c = (Jp - J0) / self.delt_evo

            cols.append(c.reshape((c.size, 1)))

            # remove delta from parameter
            self.parameters[p].value -= self.delt_evo

        return np.concatenate(cols, axis=1)

    @abstractmethod
    def get_prediction_variance(self, *args, **kwargs):
        """
        Variance of noise process of prediction from previous to current state

        :return: prediction variance
        :rtype: Array
        """
        pass

    def prediction_covar(self, *args, **kwargs):
        """
        Matrix of noise input of the prediction model from previous to current state

        :return: prediction noise
        :rtype: Array
        """
        return np.diag(self.get_prediction_variance(*args, **kwargs))


class DataModel:
    """
    Class that defines the state from dataset
    """

    def __init__(self, parameters, x_data, y_data, *args, constraints=None, delt=1e-6, **kwargs):
        """
        Creation of EstimationModel

        :param parameters: model parameters to fit
        :type parameters: lmfit.Parameters
        :param x_data: data of the x-axis, typically time of observation but can be multi-dimension
        :type x_data: Array
        :param y_data: data of the y-axis, typically observation values but can be multi-dimension
        :type y_data: Array
        :param constraints: constraints to apply on parameters, defaults to None
        :type constraints: list[dict], optional
        :param delt: step of partial derivative, defaults to 1e-6
        :type delt: float, optional
        """
        self.parameters = copy.deepcopy(parameters)
        self.n_par = len(parameters)

        self.detected_outliers_idx = []

        self.init_parameters = copy.deepcopy(parameters)
        self.init_vector = np.array([self.init_parameters[p].value for p in self.init_parameters])

        self.constraints = constraints if constraints is not None else []
        self.n_const = len(self.constraints)

        self.delt_data = delt

        self.x_data = x_data
        self.y_data = y_data
        self.n_obs = y_data.shape[0]


    """
    Defining matrices
    """

    @abstractmethod
    def get_x_values(self, *args, **kwargs):
        """
        Gets x values per observation

        :return: x values
        :rtype: Array
        """
        pass

    @abstractmethod
    def get_observations(self, *args, **kwargs):
        """
        Function of the observation vector

        :return: Vector of observations
        :rtype: Array
        """
        pass

    @abstractmethod
    def get_model(self, *args, **kwargs):
        pass

    def func_fit(self, *args, **kwargs):
        """
        Function of the residuals vector

        :return: Vector of residuals
        :rtype: Array
        """

        return self.get_observations(*args, **kwargs) - self.get_model(*args, **kwargs)

    @abstractmethod
    def get_observations_variance(self, *args, **kwargs):
        pass

    def jacobian(self, *args, **kwargs):
        """
        Function of the partial derivative matrix

        :return: Matrix of partial derivatives
        :rtype: Array
        """
        J0 = self.get_model(*args, **kwargs)
        cols = []
        for p in self.parameters:
            # adds delta to parameter
            self.parameters[p].value += self.delt_data

            Jp = self.get_model(*args, **kwargs)
            c = (Jp - J0) / self.delt_data

            cols.append(c.reshape((c.size, 1)))

            # remove delta from parameter
            self.parameters[p].value -= self.delt_data

        return np.concatenate(cols, axis=1)


    """
    Utility functions
    """

    def update_params(self, X, Qx):
        """
        Update model parameters

        :param X: vector of parameters value with similar order to :attr:parameters
        :type X: Array
        :param Qx: covariance matrix of parameters value with similar order to :attr:parameters
        :type Qx: Array
        :return:
        """
        k = 0
        for p in self.parameters:
            self.parameters[p].set(value=X[k][0])
            self.parameters[p].stderr = np.sqrt(Qx[k][k])
            k += 1

    def detect_outliers(self, detection_method, verbosity):
        """
        Detects outliers in residuals using given detection method

        :param detection_method: outliers detection method to use
        :type detection_method: method
        :param verbosity: level of verbosity (0, 1, 2)
        :type verbosity: int
        """
        out_idx = detection_method(self.func_fit())

        print("         * Detecting {} outliers with method {}".format(
            len(out_idx), detection_method
        ))
        self.detected_outliers_idx = out_idx

        if verbosity == 2:
            print("         * Outliers indices : \n {}".format(self.detected_outliers_idx))


