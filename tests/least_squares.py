import unittest
import numpy as np
from lmfit import Parameters


from estimation_toolbox.least_squares_estimation import stack_unconstrained_normal_equation
from estimation_toolbox.least_squares_models import LinearFitModel, PowerSeriesFitModel


class TestLinearLeastSquares(unittest.TestCase):

    def __init__(self, *args, **kwargs):
        super(TestLinearLeastSquares, self).__init__(*args, **kwargs)

        self.a = 12.5
        self.b = 153.6
        self.std = 1

        self.parameters = Parameters()
        self.parameters.add(name="a", value=10, min=5, max=15)
        self.parameters.add(name="b", value=100, min=50, max=300)

        self.estimated_std_factor = 1e5

        self.x1 = np.arange(0, 1000)
        self.x2 = np.arange(1000, 2000)
        self.x = np.concatenate((self.x1, self.x2))

        self.y1 = self.x1 * self.a + self.b + np.random.random(self.x1.size) * self.std
        self.y2 = self.x2 * self.a + self.b + np.random.random(self.x2.size) * self.std
        self.y = np.concatenate((self.y1, self.y2))

    def test_simple(self):

        o = LinearFitModel(self.parameters, self.x, self.y)
        self.assertIsInstance(o, LinearFitModel)
        o.linear_lse()
        self.assertAlmostEqual(o.parameters["a"].value, self.a, delta=o.parameters["a"].stderr * self.estimated_std_factor)
        self.assertAlmostEqual(o.parameters["b"].value, self.b, delta=o.parameters["b"].stderr * self.estimated_std_factor)

    def test_other_inversion_methods(self):

        print("Sparse inversion")
        sparse = LinearFitModel(self.parameters, self.x, self.y)
        sparse.linear_lse(inversion_method="sparse")

        self.assertAlmostEqual(sparse.parameters["a"].value, self.a, delta=sparse.parameters["a"].stderr * self.estimated_std_factor)
        self.assertAlmostEqual(sparse.parameters["b"].value, self.b, delta=sparse.parameters["b"].stderr * self.estimated_std_factor)

        print("QR inversion")
        qr = LinearFitModel(self.parameters, self.x, self.y)
        qr.linear_lse(inversion_method="qr")

        self.assertAlmostEqual(qr.parameters["a"].value, self.a, delta=qr.parameters["a"].stderr * self.estimated_std_factor)
        self.assertAlmostEqual(qr.parameters["b"].value, self.b, delta=qr.parameters["b"].stderr * self.estimated_std_factor)

        print("Cholesky inversion")
        chol = LinearFitModel(self.parameters, self.x, self.y)
        chol.linear_lse(inversion_method="cholesky")

        self.assertAlmostEqual(chol.parameters["a"].value, self.a, delta=chol.parameters["a"].stderr * self.estimated_std_factor)
        self.assertAlmostEqual(chol.parameters["b"].value, self.b, delta=chol.parameters["b"].stderr * self.estimated_std_factor)

    def test_stacking(self):

        print("First sample")
        o = LinearFitModel(self.parameters, self.x, self.y)
        o.linear_lse()

        print("Second sample")
        o1 = LinearFitModel(self.parameters, self.x1, self.y1)
        o1.linear_lse()
        o1.remove_constraints()

        print("All samples")
        o2 = LinearFitModel(self.parameters, self.x2, self.y2)
        o2.linear_lse()
        o2.remove_constraints()

        N, K = np.zeros((len(self.parameters), len(self.parameters))), np.zeros((len(self.parameters), 1))
        N, K = stack_unconstrained_normal_equation(N, K, o1)
        N, K = stack_unconstrained_normal_equation(N, K, o2)

        print("Stack first and second samples")
        o_bis = LinearFitModel(self.parameters, self.x, self.y)
        o_bis.inverse_unconstrained_stacked_neq(N, K)

        self.assertAlmostEqual(o.parameters["a"].value, o_bis.parameters["a"].value)
        self.assertAlmostEqual(o.parameters["b"].value, o_bis.parameters["b"].value, delta=1e-5)


class TestNonLinearLeastSquares(unittest.TestCase):

    def __init__(self, *args, **kwargs):
        super(TestNonLinearLeastSquares, self).__init__(*args, **kwargs)

        self.a0 = 12.5
        self.a1 = 153.6
        self.a2 = 45.3
        self.std = 1

        self.parameters = Parameters()
        self.parameters.add(name="a0", value=10, min=5, max=15)
        self.parameters.add(name="a1", value=100, min=50, max=300)
        self.parameters.add(name="a2", value=20, min=0, max=70)

        self.estimated_std_factor = 1e7

        self.x1 = np.arange(0, 1000)
        self.x2 = np.arange(1000, 2000)
        self.x = np.concatenate((self.x1, self.x2))

        self.y1 = (self.x1 * self.a2 + self.a1) * self.x1 + self.a0 + np.random.random(self.x1.size) * self.std
        self.y2 = (self.x2 * self.a2 + self.a1) * self.x2 + self.a0 + np.random.random(self.x2.size) * self.std
        self.y = np.concatenate((self.y1, self.y2))

    def test_simple(self):

        o = PowerSeriesFitModel(self.parameters, self.x, self.y)
        self.assertIsInstance(o, PowerSeriesFitModel)
        o.non_linear_lse()
        self.assertAlmostEqual(o.parameters["a0"].value, self.a0, delta=o.parameters["a0"].stderr * self.estimated_std_factor)
        self.assertAlmostEqual(o.parameters["a1"].value, self.a1, delta=o.parameters["a1"].stderr * self.estimated_std_factor)
        self.assertAlmostEqual(o.parameters["a2"].value, self.a2, delta=o.parameters["a2"].stderr * self.estimated_std_factor)

    def test_other_inversion_methods(self):

        print("Sparse inversion")
        sparse = PowerSeriesFitModel(self.parameters, self.x, self.y)
        sparse.non_linear_lse(inversion_method="sparse")

        self.assertAlmostEqual(sparse.parameters["a0"].value, self.a0, delta=sparse.parameters["a0"].stderr * self.estimated_std_factor)
        self.assertAlmostEqual(sparse.parameters["a1"].value, self.a1, delta=sparse.parameters["a1"].stderr * self.estimated_std_factor)
        self.assertAlmostEqual(sparse.parameters["a2"].value, self.a2, delta=sparse.parameters["a2"].stderr * self.estimated_std_factor)

        print("QR inversion")
        qr = PowerSeriesFitModel(self.parameters, self.x, self.y)
        qr.non_linear_lse(inversion_method="qr")

        self.assertAlmostEqual(qr.parameters["a0"].value, self.a0, delta=qr.parameters["a0"].stderr * self.estimated_std_factor)
        self.assertAlmostEqual(qr.parameters["a1"].value, self.a1, delta=qr.parameters["a1"].stderr * self.estimated_std_factor)
        self.assertAlmostEqual(qr.parameters["a2"].value, self.a2, delta=qr.parameters["a2"].stderr * self.estimated_std_factor)

        print("Cholesky inversion")
        chol = PowerSeriesFitModel(self.parameters, self.x, self.y)
        chol.non_linear_lse(inversion_method="cholesky")

        self.assertAlmostEqual(chol.parameters["a0"].value, self.a0, delta=chol.parameters["a0"].stderr * self.estimated_std_factor)
        self.assertAlmostEqual(chol.parameters["a1"].value, self.a1, delta=chol.parameters["a1"].stderr * self.estimated_std_factor)
        self.assertAlmostEqual(chol.parameters["a2"].value, self.a2, delta=chol.parameters["a2"].stderr * self.estimated_std_factor)

    def test_stacking(self):

        print("First sample")
        o = PowerSeriesFitModel(self.parameters, self.x, self.y)
        o.non_linear_lse()

        print("Second sample")
        o1 = PowerSeriesFitModel(self.parameters, self.x1, self.y1)
        o1.non_linear_lse()
        o1.remove_constraints()

        print("All samples")
        o2 = PowerSeriesFitModel(self.parameters, self.x2, self.y2)
        o2.non_linear_lse()
        o2.remove_constraints()

        N, K = np.zeros((len(self.parameters), len(self.parameters))), np.zeros((len(self.parameters), 1))
        N, K = stack_unconstrained_normal_equation(N, K, o1)
        N, K = stack_unconstrained_normal_equation(N, K, o2)

        print("Stack first and second samples")
        o_bis = PowerSeriesFitModel(self.parameters, self.x, self.y)
        o_bis.inverse_unconstrained_stacked_neq(N, K)

        self.assertAlmostEqual(o.parameters["a0"].value, o_bis.parameters["a0"].value, delta=2e-3)
        self.assertAlmostEqual(o.parameters["a1"].value, o_bis.parameters["a1"].value, delta=1e-5)
        self.assertAlmostEqual(o.parameters["a2"].value, o_bis.parameters["a2"].value, delta=1e-8)


if __name__ == '__main__':
    unittest.main()
